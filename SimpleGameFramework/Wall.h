#include <string>
#include "GameObject.h"
#pragma once
class Wall :public GameObject
{
public:
	Wall();
	~Wall();

public:
	std::string WallModel = "Wall.png";
public:
	
	virtual void Draw(Gdiplus::Graphics& canvas);
	virtual void Save(std::ofstream& fs);
	virtual void Load(std::ifstream& fs);
	virtual std::string GetType()
	{
		return "Wall";
	}
	virtual bool IsBlocking()
	{
		return true;
	}
};

