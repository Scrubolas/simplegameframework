#include "stdafx.h"
#include "Player.h"
#include <iostream>
#include "GameManager.h"


Player::Player()
{
	CharModel = "Player.png";
}


Player::~Player()
{
}

void Player::Draw(Gdiplus::Graphics & canvas)
{
	// Store the current transform
	Gdiplus::Matrix transform;
	canvas.GetTransform(&transform);

	// Offset drawing so we can work in local co-ords
	canvas.TranslateTransform((Gdiplus::REAL)Location.X, (Gdiplus::REAL)Location.Y);

	GameFrameworkInstance.DrawRectangle(canvas, AABBi(Vector2i(-10, -10), Vector2i(10, 10)), true, Gdiplus::Color::Blue);

	// Restore the transform
	canvas.SetTransform(&transform);
}

void Player::Update()
{
}

void Player::Save(std::ofstream & fs)
{
	// Runs "Save" from parent class
	GameObject::Save(fs);

	// Saves all variables that invlove ints on the same line
	fs << IsHoldingKey << "," << Location.X << "," << Location.Y << std::endl;

	// Saves everything else on a seperate line
	fs << CharModel << std::endl;
}

void Player::Load(std::ifstream & fs)
{
	// Runs "Load" from parent class
	GameObject::Load(fs);

	// This will take the commas that are in between each bit of data
	char DummyChar;
	// Loads all data needed
	fs >> IsHoldingKey >> DummyChar >> Location.X >> DummyChar >> Location.Y;

	fs >> CharModel;
}
