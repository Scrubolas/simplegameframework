#include <string>
#include "GameObject.h"
#pragma once
class Door :public GameObject
{
public:
	Door();
	~Door();

public:
	std::string DoorModel = "Door.png";
	bool IsClosed = true;
	bool IsLocked = false;

public:
	virtual void Draw(Gdiplus::Graphics& canvas);
	virtual void Save(std::ofstream& fs);
	virtual void Load(std::ifstream& fs);
	virtual std::string GetType()
	{
		return "Door";
	}
	virtual bool IsBlocking()
	{
		if (IsLocked == false)
		{
			return false;
		}
		else
		{
			return true;
		}
	}
};

