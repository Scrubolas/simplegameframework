#include <string>
#include "GameObject.h"
#include "GameFramework.h"
#pragma once

class Soldier :public GameObject
{
public:
	Soldier();
	~Soldier();

public:
	std::string SoldierModel = "Soldier.png";

public:
	virtual void Draw(Gdiplus::Graphics& canvas);
	void Update();
	virtual void Save(std::ofstream& fs);
	virtual void Load(std::ifstream& fs);
	virtual std::string GetType()
	{
		return "Soldier";
	}
	virtual bool IsBlocking()
	{
		return true;
	}
};

