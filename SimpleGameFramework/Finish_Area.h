#include <string>
#include "GameObject.h"
#pragma once
class Finish_Area :public GameObject
{
public:
	Finish_Area();
	~Finish_Area();

public:
	std::string FinishModel = "Finish.png";

public:
	virtual void Draw(Gdiplus::Graphics& canvas);
	virtual void Save(std::ofstream& fs);
	virtual void Load(std::ifstream& fs);
	virtual std::string GetType()
	{
		return "Finish_Area";
	}
	virtual bool IsBlocking()
	{
		return false;
	}
};

