#include "stdafx.h"
#include "GameInput.h"
#include "GameManager.h"

GameInput& GameInput::Instance()
{
	static GameInput instance;

	return instance;
}

GameInput::GameInput()
{
}

GameInput::~GameInput()
{
}

void GameInput::BeginPlay()
{
}

void GameInput::EndPlay()
{

}

void GameInput::Update(double deltaTime)
{
	Vector2i input = Vector2i::Zero;
	// The code below polls individual keys to determine if they are currently down.
	{	// WASD Key controls
		if (GetKeyState('W') & 0x8000)
		{
			if (!GameManagerInstance.Playing)
			{
				input.Y = -4;
			}
		}
		if (GetKeyState('A') & 0x8000)
		{
			input.X = -4;
		}
		if (GetKeyState('S') & 0x8000)
		{
			if (!GameManagerInstance.Playing)
			{
				input.Y = 4;
			}
		}
		if (GetKeyState('D') & 0x8000)
		{
			input.X = 4;
		}
	}
	{	// Arrow Key controls
		if (GetKeyState(VK_UP) & 0x8000)
		{
			if (!GameManagerInstance.Playing)
			{
				input.Y = -4;
			}
		}
		if (GetKeyState(VK_LEFT) & 0x8000)
		{
			input.X = -4;
		}
		if (GetKeyState(VK_RIGHT) & 0x8000)
		{
			input.X = 4;
		}
		if (GetKeyState(VK_DOWN) & 0x8000)
		{
			if (!GameManagerInstance.Playing)
			{
				input.Y = 4;
			}
		}
		if (GetKeyState(VK_SPACE) & 0x8000)
		{
			if (!GameManagerInstance.IsFalling)
			{
				input.Y = -100;
			}
		}
	}
	GameManagerInstance.CameraMovement(input);
}

void GameInput::OnKeyDown(UINT keyCode, UINT repeatCount)
{
	// NOTE: This method will not detect multiple simultaneous key presses.
	// To detect simultaneous presses you must use GetKeyState and check
	// each key of interest.

	switch (keyCode)
	{
	case 'W':
		break;
	case 'A':
		break;
	case 'S':
		break;
	case 'D':
		break;
	case 'Q':
		break;
	case 'E':
		break;
	case 'F':
		break;
	case 'C':
		break;
	case '0':
		GameManagerInstance.SelectedObjType = "Player";
		break;
	case '1':
		GameManagerInstance.SelectedObjType = "Soldier";
		break;
	case '2':
		GameManagerInstance.SelectedObjType = "Turret";
		break;
	case '3':
		GameManagerInstance.SelectedObjType = "Wall";
		break;
	case '4':
		GameManagerInstance.SelectedObjType = "Door";
		break;
	case '5':
		GameManagerInstance.SelectedObjType = "Ammo";
		break;
	case '6':
		GameManagerInstance.SelectedObjType = "Key";
		break;
	case '7':
		GameManagerInstance.SelectedObjType = "Finish_Area";
		break;
	case 'K':
		if (!GameManagerInstance.Playing)
		{
			GameManagerInstance.SaveLevel("Level.Level");
		}
			break;
	case 'L':
		if (!GameManagerInstance.Playing)
		{
			GameManagerInstance.LoadLevel("Level.Level");
		}
		break;
	case VK_TAB:
		GameManagerInstance.ChangeMode();
		if (GameManagerInstance.Playing)
		{
			GameManagerInstance.SaveLevel("Temp.Level");
		}
		else
		{
			GameManagerInstance.LoadLevel("Temp.Level");
		}
		break;

	case VK_LEFT:
		break;
	case VK_RIGHT:
		break;
	case VK_UP:
		break;
	case VK_DOWN:
		break;

	case VK_CONTROL:
		break;
	case VK_SPACE:
		break;
	case VK_SHIFT:
		break;

	case VK_F1:
		break;
	case VK_F2:
		break;
	case VK_F3:
		break;
	case VK_F4:
		break;
	case VK_F5:
		break;
	case VK_F6:
		break;
	case VK_F7:
		break;
	case VK_F8:
		break;
	case VK_F9:
		break;
	case VK_F10:
		break;
	case VK_F11:
		break;
	case VK_F12:
		break;
	}
}

void GameInput::OnKeyUp(UINT keyCode, UINT repeatCount)
{
	// NOTE: This method will not detect multiple simultaneous key presses.
	// To detect simultaneous presses you must use GetKeyState and check
	// each key of interest.

	switch (keyCode)
	{
	case 'W':
		break;
	case 'A':
		break;
	case 'S':
		break;
	case 'D':
		break;
	case 'Q':
		break;
	case 'E':
		break;
	case 'F':
		break;
	case 'C':
		break;

	case VK_LEFT:
		break;
	case VK_RIGHT:
		break;
	case VK_UP:
		break;
	case VK_DOWN:
		break;

	case VK_CONTROL:
		break;
	case VK_SPACE:
		break;
	case VK_SHIFT:
		break;

	case VK_F1:
		break;
	case VK_F2:
		break;
	case VK_F3:
		break;
	case VK_F4:
		break;
	case VK_F5:
		break;
	case VK_F6:
		break;
	case VK_F7:
		break;
	case VK_F8:
		break;
	case VK_F9:
		break;
	case VK_F10:
		break;
	case VK_F11:
		break;
	case VK_F12:
		break;
	}
}

void GameInput::OnLButtonDown(const Vector2i& point)
{
	DebugLog("OnLButtonDown at " << point.X << "," << point.Y);

	if (GameManagerInstance.Playing == false)
	{
		GameManagerInstance.LeftButtonDown(point);
	}
}

void GameInput::OnLButtonUp(const Vector2i& point)
{
	DebugLog("OnLButtonUp at " << point.X << "," << point.Y);
}

void GameInput::OnLButtonDblClk(const Vector2i& point)
{
	DebugLog("OnLButtonDblClk at " << point.X << "," << point.Y);
}

void GameInput::OnRButtonDown(const Vector2i& point)
{
	DebugLog("OnRButtonDown at " << point.X << "," << point.Y);
	if (GameManagerInstance.Playing == false)
	{
		GameManagerInstance.RightButtonDown(point);
	}
}

void GameInput::OnRButtonUp(const Vector2i& point)
{
	DebugLog("OnRButtonUp at " << point.X << "," << point.Y);
}

void GameInput::OnRButtonDblClk(const Vector2i& point)
{
	DebugLog("OnRButtonDblClk at " << point.X << "," << point.Y);
}

void GameInput::OnMouseMove(const Vector2i& point)
{
	//if (isLMBDown == true)
	//{
	//	GameManagerInstance.LeftButtonDown(point);
	//}
	//if (isRMBDown == true)
	//{
	//	GameManagerInstance.RightButtonDown(point);
	//}
}
