#include "stdafx.h"
#include "Wall.h"


Wall::Wall()
{
	WallModel = "Wall.png";
}


Wall::~Wall()
{
}

void Wall::Draw(Gdiplus::Graphics& canvas)
{
	// Store the current transform
	Gdiplus::Matrix transform;
	canvas.GetTransform(&transform);

	// Offset drawing so we can work in local co-ords
	canvas.TranslateTransform((Gdiplus::REAL)Location.X, (Gdiplus::REAL)Location.Y);

	GameFrameworkInstance.DrawRectangle(canvas, AABBi(Vector2i(-10, -10), Vector2i(10, 10)), true, Gdiplus::Color::Black);

	// Restore the transform
	canvas.SetTransform(&transform);
}

void Wall::Save(std::ofstream & fs)
{
	// Runs "Save" from parent class
	GameObject::Save(fs);

	// Save all necassary data
	fs << Location.X << "," << Location.Y << std::endl;
	fs << WallModel << std::endl;
}

void Wall::Load(std::ifstream & fs)
{
	// Runs "Load" from parent class
	GameObject::Load(fs);

	char DummyChar;

	fs >> Location.X >> DummyChar >> Location.Y;

	fs >> WallModel;
}
