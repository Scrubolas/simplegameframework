#include "stdafx.h"
#include "GameManager.h"

GameManager& GameManager::Instance()
{
	static GameManager instance;

	return instance;
}

GameManager::GameManager()
{
}

GameManager::~GameManager()
{
}


void GameManager::BeginPlay()
{
	////////////////////////////////////////////////////////////////////////////////
	// Begin example code

	// Load the image file Untitled.png from the Images folder. Give it the unique name of Image1
	 GameFrameworkInstance.LoadImageResource(AppConfigInstance.GetResourcePath("Images/Background.png"), "Background");
	 //GameFrameworkInstance.LoadImageResource(AppConfigInstance.GetResourcePath("Background.jpg"), "Background");
	 Gravity.Y = -4;
	 
}

void GameManager::EndPlay()
{
	delete ObjectPtr;

	for (GameObject* objPtr : LevelObjects)
	{
		delete objPtr;
	}
}

void GameManager::Update(double deltaTime)
{

}

void GameManager::Render(Gdiplus::Graphics& canvas, const CRect& clientRect)
{
	////////////////////////////////////////////////////////////////////////////////
	// Begin example code

	// Save the current transformation of the scene
	Gdiplus::Matrix transform;
	canvas.GetTransform(&transform);

	canvas.TranslateTransform((Gdiplus::REAL) LevelOffset.X, (Gdiplus::REAL) LevelOffset.Y);

	ImageWrapper* image = GameFrameworkInstance.GetLoadedImage("Background");
	GameFrameworkInstance.DrawImage(canvas, Vector2i::Zero, image);

	// Draw all level objects

	for (GameObject* objtoDraw : LevelObjects)
	{
		objtoDraw->Draw(canvas);
	}

	// Restore the transformation of the scene
	canvas.SetTransform(&transform);

	// Draw UI here, so it doesn't move

	// End example code
	////////////////////////////////////////////////////////////////////////////////
}

void GameManager::LeftButtonDown(const Vector2i & point)
{
	// Creates a grid like structure to place objects on
	Vector2i SnappedLocation;
	SnappedLocation.X = 20 * ((point.X - LevelOffset.X) / 20);
	SnappedLocation.Y = 20 * ((point.Y - LevelOffset.Y) / 20);

	// Check if we clicked on another GameObject
	GameObject* foundObject = nullptr;
	for (GameObject* objtoTest : LevelObjects)
	{
		//Construct our bounds for the click
		AABBi bounds(objtoTest->Location - Vector2i(10, 10), objtoTest->Location + Vector2i(10, 10));

		// Did we click in the bounds of the object
		if (bounds.Contains(point))
		{
			foundObject = objtoTest;
			break;
		}
	}

	if (foundObject == nullptr)
	{
		if (GameManagerInstance.SelectedObjType == "Player")
		{
			if (PlayerPlaced == false)
			{
				Player* ObjToDraw = new Player();
				ObjToDraw->Location = SnappedLocation;
				LevelObjects.push_back(ObjToDraw);
				PlayerPtr = ObjToDraw;
				PlayerPlaced = true;
			}
		}
		if (GameManagerInstance.SelectedObjType == "Soldier")
		{
			Soldier* ObjToDraw = new Soldier();
			ObjToDraw->Location = SnappedLocation;
			LevelObjects.push_back(ObjToDraw);
		}
		if (GameManagerInstance.SelectedObjType == "Turret")
		{
			Turret* ObjToDraw = new Turret();
			ObjToDraw->Location = SnappedLocation;
			LevelObjects.push_back(ObjToDraw);
		}
		if (GameManagerInstance.SelectedObjType == "Wall")
		{
			Wall* ObjToDraw = new Wall();
			ObjToDraw->Location = SnappedLocation;
			LevelObjects.push_back(ObjToDraw);
		}
		if (GameManagerInstance.SelectedObjType == "Door")
		{
			Door* ObjToDraw = new Door();
			ObjToDraw->Location = SnappedLocation;
			LevelObjects.push_back(ObjToDraw);
		}
		if (GameManagerInstance.SelectedObjType == "Ammo")
		{
			Ammo* ObjToDraw = new Ammo();
			ObjToDraw->Location = SnappedLocation;
			LevelObjects.push_back(ObjToDraw);
		}
		if (GameManagerInstance.SelectedObjType == "Key")
		{
			Key* ObjToDraw = new Key();
			ObjToDraw->Location = SnappedLocation;
			LevelObjects.push_back(ObjToDraw);
		}
		if (GameManagerInstance.SelectedObjType == "Finish_Area")
		{
			if (FinishAreaPlaced == false)
			{
				Finish_Area* ObjToDraw = new Finish_Area();
				ObjToDraw->Location = SnappedLocation;
				LevelObjects.push_back(ObjToDraw);
				FinishAreaPlaced = true;
			}
		}
		++NoGameObjects;
	}

}

void GameManager::RightButtonDown(const Vector2i & point)
{
	// Snapps the mouse location to where the object should be on the grid to delete
	Vector2i SnappedLocation;
	SnappedLocation.X = 20 * ((point.X - LevelOffset.X) / 20);
	SnappedLocation.Y = 20 * ((point.Y - LevelOffset.Y) / 20);

	// Check if we clicked on another GameObject
	GameObject* foundObject = nullptr;
	for (GameObject* objtoTest : LevelObjects)
	{
		//Construct our bounds for the click
		AABBi bounds(objtoTest->Location - Vector2i(10, 10), objtoTest->Location + Vector2i(10, 10));

		// Did we click in the bounds of the object
		if (bounds.Contains(point))
		{
			foundObject = objtoTest;
			break;
		}
	}

	// Clicked on something check
	if (foundObject)
	{
		if (dynamic_cast<Player*>(foundObject))
		{
			PlayerPlaced = false;
			PlayerPtr = nullptr;
		}
		if (dynamic_cast<Finish_Area*>(foundObject))
		{
			FinishAreaPlaced = false;
		}
		LevelObjects.remove(foundObject);
		delete foundObject;
		return;
	}
}

void GameManager::CameraMovement(const Vector2i & input)
{
	if (Playing == false)
	{
		LevelOffset -= input;
	}
	if (Playing == true)
	{
		bool CanMove = true;
		Vector2i NewLocation = PlayerPtr->Location + input;
		Vector2i NewCamLocation = LevelOffset - input;
		Vector2i NewLocationGravity = NewLocation - Gravity;
		Vector2i NewCamLocationGravity = NewCamLocation + Gravity;
		AABBi PlayerCol(NewLocation - Vector2i(10, 10), NewLocation + Vector2i(10, 10));
		AABBi PlayerGravCol(NewLocationGravity - Vector2i(10, 10), NewLocationGravity + Vector2i(10, 10));
		for (GameObject* ObjtoTest : LevelObjects)
		{
			if (ObjtoTest->IsBlocking() == true)
			{
				AABBi ObjCol(ObjtoTest->Location - Vector2i(10, 10), ObjtoTest->Location + Vector2i(10, 10));
				if (PlayerGravCol.Intersects(ObjCol))
				{
					if (PlayerCol.Intersects(ObjCol))
					{
						CanMove = false;
						IsFalling = false;
						break;
					}
					else
					{
						NewLocationGravity = NewLocation;
						NewCamLocationGravity = NewCamLocation;
						IsFalling = false;
					}
				}
				else
				{
					IsFalling = true;
				}
			}
			else if (ObjtoTest->GetType() == "Finish_Area")
			{
				AABBi ObjCol(ObjtoTest->Location - Vector2i(10, 10), ObjtoTest->Location + Vector2i(10, 10));
				if (PlayerCol.Intersects(ObjCol) || PlayerGravCol.Intersects(ObjCol))
				{
					Playing = false;
					LoadLevel("Temp.Level");
					break;
				}
			}
			else if (ObjtoTest->GetType() == "Key")
			{
				AABBi ObjCol(ObjtoTest->Location - Vector2i(10, 10), ObjtoTest->Location + Vector2i(10, 10));
				if (PlayerCol.Intersects(ObjCol) || PlayerGravCol.Intersects(ObjCol))
				{
					if (!PlayerPtr->IsHoldingKey)
					{
						PlayerPtr->IsHoldingKey = true;
						LevelObjects.remove(ObjtoTest);
					}
					break;
				}
			}
			if (ObjtoTest->GetType() == "Soldier" || ObjtoTest->GetType() == "Turret")
			{
				AABBi ObjCol(ObjtoTest->Location - Vector2i(10, 10), ObjtoTest->Location + Vector2i(10, 10));
				if (PlayerCol.Intersects(ObjCol) || PlayerGravCol.Intersects(ObjCol))
				{
					Playing = false;
					LoadLevel("Temp.Level");
					break;
				}
			}
		}
		if (CanMove == true)
		{
			LevelOffset = NewCamLocationGravity;
			PlayerPtr->Location = NewLocationGravity;
		}
	}
}

void GameManager::ChangeMode()
{
	if (PlayerPlaced == true)
	{
		Playing = !Playing;
	}
}


void GameManager::SaveLevel(std::string FileName)
{
	std::ofstream savefile(FileName);

	savefile << NoGameObjects << std::endl;

	for (GameObject* ObjToSave : LevelObjects)
	{
		ObjToSave->Save(savefile);
	}

	savefile.close();
}

void GameManager::LoadLevel(std::string FileName)
{
	for (GameObject* objPtr : LevelObjects)
	{
		delete objPtr;
	}
	LevelObjects.clear();

	std::ifstream LoadFile(FileName);
	int NumGameObjToLoad = 0;
	LoadFile >> NumGameObjToLoad;
	NoGameObjects = NumGameObjToLoad;

	for (int index = 0; index < NumGameObjToLoad; ++index)
	{
		std::string GameObjectName;
		LoadFile >> GameObjectName;

		GameObject* ObjectPtr = nullptr;

		if (GameObjectName == "Player")
		{
			ObjectPtr = new Player();
			PlayerPlaced = true;
			PlayerPtr = (Player*)ObjectPtr;
		}
		if (GameObjectName == "Soldier")
		{
			ObjectPtr = new Soldier();
		}
		if (GameObjectName == "Ammo")
		{
			ObjectPtr = new Ammo();
		}
		if (GameObjectName == "Door")
		{
			ObjectPtr = new Door();
		}
		if (GameObjectName == "Finish_Area")
		{
			ObjectPtr = new Finish_Area();
		}
		if (GameObjectName == "Key")
		{
			ObjectPtr = new Key();
		}
		if (GameObjectName == "Turret")
		{
			ObjectPtr = new Turret();
		}
		if (GameObjectName == "Wall")
		{
			ObjectPtr = new Wall();
		}

		if (ObjectPtr)
		{
			ObjectPtr->Load(LoadFile);
			LevelObjects.push_back(ObjectPtr);
		}
	}
}
