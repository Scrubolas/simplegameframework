#pragma once

#include "GameFramework.h"
#include "GameObject.h"
#include "Player.h"
#include "Ammo.h"
#include "Door.h"
#include "Finish_Area.h"
#include "Key.h"
#include "Soldier.h"
#include "Turret.h"
#include "Wall.h"
#include "GameInput.h"
#include <list>

class GameManager
{
public:
	static GameManager& Instance();

	/** BeginPlay is called when the game first launches. It is only called once per launch. */
	void BeginPlay();

	/** EndPlay is called when the game is closing down. It is only called once and only on exit. */
	void EndPlay();

	/**
	 * Update is called every frame. Update is always called before render
	 *
	 * @param deltaTime The time in seconds since Update was last called.
	 */
	void Update(double deltaTime);

	/**
	 * Render is called every frame after Update is called. All drawing must happen in render.
	 *
	 * @param [in,out] canvas The canvas.
	 * @param clientRect	  The rectangle representing the drawable area.
	 */
	void Render(Gdiplus::Graphics& canvas, const CRect& clientRect);

	void LeftButtonDown(const Vector2i& point);

	void RightButtonDown(const Vector2i& point);

	void CameraMovement(const Vector2i& input);

	void ChangeMode();

	void SaveLevel(std::string FileName);

	void LoadLevel(std::string FileName);

	bool PlayerPlaced = false;

	bool FinishAreaPlaced = false;

	std::string SelectedObjType;

	int NoGameObjects;

	bool Playing = false;

	Player* PlayerPtr;

	bool IsFalling;

private:
	GameManager();
	~GameManager();

	GameObject* ObjectPtr;

	std::list<GameObject*> LevelObjects;

	Vector2i LevelOffset;

	std::string FileName;

	Vector2i Gravity;
};

/**
 * Retrieves the GameManagerInstance for the game.
 *
 * @return The game manager instance
 */
#define GameManagerInstance (GameManager::Instance())
