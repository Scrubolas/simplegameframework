#include <string>
#include "GameObject.h"
#include "GameFramework.h"
#pragma once

class Player :public GameObject
{
public:
	Player();
	virtual ~Player();

public:
	bool IsHoldingKey = false;
	std::string CharModel = "Player.png";

public:
	virtual void Draw(Gdiplus::Graphics& canvas);
	void Update();
	virtual void Save(std::ofstream& fs);
	virtual void Load(std::ifstream& fs);
	virtual std::string GetType()
	{
		return "Player";
	}
	virtual bool IsBlocking()
	{
		return false;
	}
};

