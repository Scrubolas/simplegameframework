#include "stdafx.h"
#include "GameObject.h"
#include "GameManager.h"

GameObject::GameObject()
{
}


GameObject::~GameObject()
{
}

void GameObject::Draw(Gdiplus::Graphics & canvas)
{
	// Store the current transform
	Gdiplus::Matrix transform;
	canvas.GetTransform(&transform);

	// Offset drawing so we can work in local co-ords
	canvas.TranslateTransform((Gdiplus::REAL)Location.X, (Gdiplus::REAL)Location.Y);

	// Restore the transform
	canvas.SetTransform(&transform);
}

void GameObject::Update(double deltaTime)
{
}

void GameObject::Save(std::ofstream & fs)
{
	fs << GetType() << std::endl;
}

void GameObject::Load(std::ifstream & fs)
{
}
