#include <string>
#include "GameObject.h"
#pragma once
class Key :public GameObject
{
public:
	Key();
	~Key();

public:
	std::string KeyModel = "Key.png";

public:
	virtual void Draw(Gdiplus::Graphics& canvas);
	virtual void Save(std::ofstream& fs);
	virtual void Load(std::ifstream& fs);
	virtual std::string GetType()
	{
		return "Key";
	}
	virtual bool IsBlocking()
	{
		return false;
	}
};

