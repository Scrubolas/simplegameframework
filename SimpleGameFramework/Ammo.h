#include <string>
#include "GameObject.h"
#pragma once
class Ammo :public GameObject
{
public:
	Ammo();
	~Ammo();

public:
	std::string AmmoModel = "Ammo.png";

public:
	virtual void Draw(Gdiplus::Graphics& canvas);
	virtual void Save(std::ofstream& fs);
	virtual void Load(std::ifstream& fs);
	virtual std::string GetType()
	{
		return "Ammo";
	}
	virtual bool IsBlocking()
	{
		return false;
	}
};

