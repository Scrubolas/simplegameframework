#include "stdafx.h"
#include "Door.h"


Door::Door()
{
	DoorModel = "Door.png";
}


Door::~Door()
{
}

void Door::Draw(Gdiplus::Graphics& canvas)
{
	// Store the current transform
	Gdiplus::Matrix transform;
	canvas.GetTransform(&transform);

	// Offset drawing so we can work in local co-ords
	canvas.TranslateTransform((Gdiplus::REAL)Location.X, (Gdiplus::REAL)Location.Y);

	GameFrameworkInstance.DrawRectangle(canvas, AABBi(Vector2i(-10, -10), Vector2i(10, 10)), true, Gdiplus::Color::Brown);

	// Restore the transform
	canvas.SetTransform(&transform);
}

void Door::Save(std::ofstream & fs)
{
	// Runs "Save" from parent class
	GameObject::Save(fs);

	// Saves all necassay data
	fs << IsLocked << "," << IsClosed << "," << Location.X << "," << Location.Y << std::endl;
	fs << DoorModel << std::endl;
}

void Door::Load(std::ifstream & fs)
{
	// Runs "Load" from parent class
	GameObject::Load(fs);
	char DummyChar;
	// Loads all necassary data in
	fs >> IsLocked >> DummyChar >> IsClosed >> DummyChar
		>> Location.X >> DummyChar >> Location.Y;
	fs >> DoorModel;
}
