#include "stdafx.h"
#include "Soldier.h"
#include "GameManager.h"


Soldier::Soldier()
{
	SoldierModel = "Soldier.png";
}


Soldier::~Soldier()
{
}

void Soldier::Draw(Gdiplus::Graphics& canvas)
{
	// Store the current transform
	Gdiplus::Matrix transform;
	canvas.GetTransform(&transform);

	// Offset drawing so we can work in local co-ords
	canvas.TranslateTransform((Gdiplus::REAL)Location.X, (Gdiplus::REAL)Location.Y);

	GameFrameworkInstance.DrawRectangle(canvas, AABBi(Vector2i(-10, -10), Vector2i(10, 10)), true, Gdiplus::Color::Red);

	canvas.SetTransform(&transform);
}

void Soldier::Update()
{
}

void Soldier::Save(std::ofstream & fs)
{
	// Runs "Save" from parent class
	GameObject::Save(fs);

	// Saves all neccasary data
	fs << Location.X << "," << Location.Y << std::endl;
	fs << SoldierModel << std::endl;
}

void Soldier::Load(std::ifstream & fs)
{
	// Runs "Load" from parent class
	GameObject::Load(fs);

	// This will take the commas that are in between each bit of data
	char DummyChar;
	// Loads all necassary data in
	fs >> Location.X >> DummyChar >> Location.Y;
	fs >> SoldierModel;
}
