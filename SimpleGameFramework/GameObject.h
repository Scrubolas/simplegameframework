#pragma once

#include <string>
#include <iostream>
#include <fstream>
#include "GameFramework.h"

class GameObject
{
public:
	GameObject();
	virtual ~GameObject();

	virtual void Draw(Gdiplus::Graphics& canvas);

	void Update(double deltaTime);

	virtual std::string GetType()
	{
		return "ERROR";
	}
	
	Vector2i Location;

public:
	virtual void Save(std::ofstream& fs);
	virtual void Load(std::ifstream& fs);
	virtual bool IsBlocking()
	{
		return false;
	}
};

