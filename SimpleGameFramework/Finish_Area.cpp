#include "stdafx.h"
#include "Finish_Area.h"


Finish_Area::Finish_Area()
{
	FinishModel = "Finish.png";
}


Finish_Area::~Finish_Area()
{
}

void Finish_Area::Draw(Gdiplus::Graphics& canvas)
{
	// Store the current transform
	Gdiplus::Matrix transform;
	canvas.GetTransform(&transform);

	// Offset drawing so we can work in local co-ords
	canvas.TranslateTransform((Gdiplus::REAL)Location.X, (Gdiplus::REAL)Location.Y);

	GameFrameworkInstance.DrawRectangle(canvas, AABBi(Vector2i(-10, -10), Vector2i(10, 10)), true, Gdiplus::Color::Green);

	// Restore the transform
	canvas.SetTransform(&transform);
}

void Finish_Area::Save(std::ofstream & fs)
{
	// Runs "Save" from parent class
	GameObject::Save(fs);

	// Save all necassary data
	fs << Location.X << "," << Location.Y << std::endl;
	fs << FinishModel << std::endl;
}

void Finish_Area::Load(std::ifstream & fs)
{
	// Runs "Load" from parent class
	GameObject::Load(fs);
	char DummyChar;
	// Loads all necassary data in
	fs >> Location.X >> DummyChar >> Location.Y;
	fs >> FinishModel;
}
