#include "stdafx.h"
#include "Ammo.h"


Ammo::Ammo()
{
	AmmoModel = "Ammo.png";
}


Ammo::~Ammo()
{
}

void Ammo::Draw(Gdiplus::Graphics& canvas)
{
	// Store the current transform
	Gdiplus::Matrix transform;
	canvas.GetTransform(&transform);

	// Offset drawing so we can work in local co-ords
	canvas.TranslateTransform((Gdiplus::REAL)Location.X, (Gdiplus::REAL)Location.Y);

	GameFrameworkInstance.DrawRectangle(canvas, AABBi(Vector2i(-10, -10), Vector2i(10, 10)), true, Gdiplus::Color::PaleGoldenrod);

	// Restore the transform
	canvas.SetTransform(&transform);
}

void Ammo::Save(std::ofstream & fs)
{
	// Runs "Save" from parent class
	GameObject::Save(fs);

	// Saves all necassary data
	fs << Location.X << "," << Location.Y << std::endl;
	fs << AmmoModel << std::endl;
}

void Ammo::Load(std::ifstream & fs)
{
	// Runs "Load" from parent class
	GameObject::Load(fs);
	char DummyChar;
	// Loads all necassary data in
	fs >> Location.X >> DummyChar >> Location.Y;
	fs >> AmmoModel;
}
