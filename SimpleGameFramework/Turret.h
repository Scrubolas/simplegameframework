#include <string>
#include "GameObject.h"
#pragma once
class Turret :public GameObject
{
public:
	Turret();
	~Turret();

public:
	std::string TurretModel = "Turret.png";

public:
	virtual void Draw(Gdiplus::Graphics& canvas);
	void Update();
	virtual void Save(std::ofstream& fs);
	virtual void Load(std::ifstream& fs);
	virtual std::string GetType()
	{
		return "Turret";
	}
	virtual bool IsBlocking()
	{
		return true;
	}
};